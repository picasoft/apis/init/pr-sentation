all:
	pdflatex main.tex
	pdflatex main.tex

clean:
	rm -f *.log *.toc *.nav *~ *.out *.aux *.pdf *.snm *.swp *.vrb
