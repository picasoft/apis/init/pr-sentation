# Jour 1 : Présentation de Picasoft

Ce document a pour but d'être un support afin de présenter l'association Picasoft en 5/10 minutes environ. Il a été conçu pour l'API Init mais peut servir de modèle pour d'autres présentations.

Le document est généré automatiquement à partir du fichier `main.tex` présent à la racine par une chaîne d'intégration. Une fois généré avec succès, il est publié automatiquement à [cette adresse](https://uploads.picasoft.net/api/presentation.pdf).